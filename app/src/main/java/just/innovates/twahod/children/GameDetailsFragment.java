package just.innovates.twahod.children;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import cn.pedant.SweetAlert.SweetAlertDialog;
import just.innovates.twahod.R;
import just.innovates.twahod.base.SweetDialogs;
import just.innovates.twahod.base.models.Game;
import just.innovates.twahod.base.models.Question;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class GameDetailsFragment extends Fragment {
    private View rootView;
    private ImageView answer1, answer2, answer3, answer4;
    private int currentQuestion=0;
    private int choosedAnswer=0;
    private Question question;
    private Game game;
    private Button add;
    private TextView questionText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_game_details, container, false);

        game = (Game) getArguments().getSerializable("game");

        answer1 = rootView.findViewById(R.id.answer1);
        answer2 = rootView.findViewById(R.id.answer2);
        answer3 = rootView.findViewById(R.id.answer3);
        answer4 = rootView.findViewById(R.id.answer4);
        questionText = rootView.findViewById(R.id.question);
        add = rootView.findViewById(R.id.add);


        answer1.setOnClickListener(view -> {
            choosedAnswer=0;
            isCorrect();
        });
        answer2.setOnClickListener(view -> {
            choosedAnswer=1;
            isCorrect();
        });

        answer3.setOnClickListener(view -> {
            choosedAnswer=2;
            isCorrect();
        });

        answer4.setOnClickListener(view -> {
            choosedAnswer=3;
            isCorrect();
        });

        setImages();

        add.setOnClickListener(view -> {
            currentQuestion++;
            setImages();
        });


        return rootView;
    }

    private void setImages() {
        try {
            question = game.questions.get(currentQuestion);
            questionText.setText(question.question);
            answer1.setImageBitmap(null);
            answer2.setImageBitmap(null);
            answer3.setImageBitmap(null);
            answer4.setImageBitmap(null);
            ConnectionHelper.loadImage(answer1, question.firstAnswer);
            ConnectionHelper.loadImage(answer2, question.secondAnswer);
            ConnectionHelper.loadImage(answer3, question.thirdAnswer);
            ConnectionHelper.loadImage(answer4, question.forthAnswer);

        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void isCorrect(){
        if(choosedAnswer==question.correctAnswer) {
            SweetDialogs.successMessage(getActivity(), "ممتاز !!! اجابه صحيحه ", new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    add.performClick();
                }
            });

        }else {
            SweetDialogs.errorMessage(getActivity(), "خطا !!! اجابه خاطئه ");
        }
    }


}
