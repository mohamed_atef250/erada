package just.innovates.twahod.children;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Game;
import just.innovates.twahod.base.models.Question;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.ViewHolder> {

    Context context;
    ArrayList<Game> games;

    public GamesAdapter(Context context) {
        this.context = context;
        games = DataBaseHelper.getDataLists().games;
        if (games == null) {
            games = new ArrayList<>();
        }
    }


    @Override
    public GamesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game, parent, false);
        GamesAdapter.ViewHolder viewHolder = new GamesAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GamesAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.image,"https://static.wikia.nocookie.net/laracroft/images/3/3a/2018_Movie_Lara.png/revision/latest/top-crop/width/360/height/360?cb=20170720151122");
       // holder.name.setText(games.get(position).name);


        holder.itemView.setOnClickListener(view -> {

            Game game = new Game("1","من هيا تاره","https://static.wikia.nocookie.net/laracroft/images/3/3a/2018_Movie_Lara.png/revision/latest/top-crop/width/360/height/360?cb=20170720151122");
              ArrayList<Question> questions = new ArrayList<>();
            game.addQuestion(new Question("1","من هيا تاره","https://miro.medium.com/max/3840/0*4frrHEPaUymSJMyi.jpg"
                    ,"https://images.freecreatives.com/wp-content/uploads/2015/08/stunning-3d-girl-wallpaper.jpg",
                    "https://onewallpaperonline.files.wordpress.com/2011/09/3d-girls-wallpaper-5.jpg",
                    "https://image.slidesharecdn.com/fantastic3dgirls-090505182725-phpapp01/95/fantastic-3d-girls-1-728.jpg?cb=1241569094",2,"2"));
            GameDetailsFragment gameDetailsFragment = new GameDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("game", game);
            gameDetailsFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(), gameDetailsFragment, "ChatFragment");
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);


        }
    }
}
