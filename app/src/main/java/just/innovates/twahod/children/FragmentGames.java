package just.innovates.twahod.children;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.R;


public class FragmentGames extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_games, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        GamesAdapter menuAdapter = new GamesAdapter(getActivity());
        menuList.setAdapter(menuAdapter);

        rootView.findViewById(R.id.tv_systems_add).setVisibility(View.GONE);


        return rootView;
    }


}
