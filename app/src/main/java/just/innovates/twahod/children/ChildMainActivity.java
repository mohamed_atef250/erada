package just.innovates.twahod.children;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;

public class ChildMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_main);

        ChipNavigationBar morphBottomNavigationView = findViewById(R.id.bottomNavigationView);
        morphBottomNavigationView.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int i) {
                if (i == R.id.games) {
                    FragmentHelper.replaceFragment(ChildMainActivity.this, new FragmentGames(), "FragmentGames");

                } else if (i == R.id.video) {
                    FragmentHelper.replaceFragment(ChildMainActivity.this, new FragmentVideos(), "FragmentVideos");
                }
            }
        });


        morphBottomNavigationView.setItemSelected(R.id.video, true);


    }
}
