package just.innovates.twahod.children;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.admin.FragmentAddVideo;
import just.innovates.twahod.admin.VideosAdapter;


public class FragmentVideos extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_videos, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        VideosAdapter menuAdapter = new VideosAdapter(getActivity(),getArguments().getString("type"),false);
        menuList.setAdapter(menuAdapter);

        rootView.findViewById(R.id.tv_systems_add).setVisibility(View.GONE);


        return rootView;
    }


}
