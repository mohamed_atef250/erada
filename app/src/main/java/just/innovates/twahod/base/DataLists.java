package just.innovates.twahod.base;


import java.util.ArrayList;

import just.innovates.twahod.base.models.Ads;
import just.innovates.twahod.base.models.Center;
import just.innovates.twahod.base.models.Comment;
import just.innovates.twahod.base.models.Expert;
import just.innovates.twahod.base.models.Video;
import just.innovates.twahod.base.models.Chat;
import just.innovates.twahod.base.models.Question;
import just.innovates.twahod.base.models.Child;
import just.innovates.twahod.base.models.Game;
import just.innovates.twahod.base.models.User;

public class DataLists {

    public ArrayList<User> users = new ArrayList<>();
    public ArrayList<Question> companies = new ArrayList<>();
    public ArrayList<Expert> experts = new ArrayList<>();
    public ArrayList<Game> games= new ArrayList<>();
    public ArrayList<Child> children= new ArrayList<>();
    public ArrayList<Video> videos= new ArrayList<>();
    public ArrayList<Chat> chats= new ArrayList<>();
    public ArrayList<Comment>comments= new ArrayList<>();
    public ArrayList<Ads> ads = new ArrayList<>();
    public ArrayList<Center>centers = new ArrayList<Center>();

}
