package just.innovates.twahod.base.models;

import java.io.Serializable;

public class Chat implements Serializable {

    public User user;
    public User admin;
    public String message;
    public String type;

    public Chat(User user, User admin, String message, String type) {
        this.user = user;
        this.admin = admin;
        this.message = message;
        this.type=type;
    }
}
