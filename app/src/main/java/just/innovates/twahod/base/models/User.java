package just.innovates.twahod.base.models;

import java.io.Serializable;

import just.innovates.twahod.base.DataBaseHelper;

public class User implements Serializable {
    public int id;
    public String  name, userName, image, phone, email, password,type;
    public String special,job;

    public User( String name, String userName, String phone, String email, String password, String image,String type) {
        this.id = DataBaseHelper.getCounter();
        this.name = name;
        this.image = image;
        this.userName = userName;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.type =type;
    }
}
