package just.innovates.twahod.base.models;


import java.io.Serializable;

import just.innovates.twahod.base.DataBaseHelper;

public class Child implements Serializable {

    public String id ,name, userName, email, phone, state, type, password,image;

    public Child(String id,String name, String userName, String email, String phone, String state, String type, String password,String image) {
        this.name = name;
        this.userName = userName;
        this.email = email;
        this.phone = phone;
        this.state = state;
        this.type = type;
        this.password = password;
        this.image = image;
        this.id= id;
    }
}
