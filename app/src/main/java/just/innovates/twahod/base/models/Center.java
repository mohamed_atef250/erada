package just.innovates.twahod.base.models;

import java.io.Serializable;

import just.innovates.twahod.base.DataBaseHelper;

public class Center implements Serializable {

    public int id;
    public  String name,city,about,image;
    public double lat,lng;

    public Center( String name, String city, String about,  double lat, double lng,String image) {
        this.id = DataBaseHelper.getCounter();
        this.name = name;
        this.city = city;
        this.about = about;
        this.lat=lat;
        this.lng=lng;
        this.image=image;


    }
}
