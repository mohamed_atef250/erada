package just.innovates.twahod.base;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
