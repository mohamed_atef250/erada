package just.innovates.twahod;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import just.innovates.twahod.children.FragmentVideos;
import just.innovates.twahod.mother.FragmentCenters;
import just.innovates.twahod.mother.FragmentMotherSpecializations;

public class ClerkMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_main);

        FragmentHelper.addFragment(ClerkMainActivity.this, new FragmentAds(), "FragmentAds");

        findViewById(R.id.iv_main_menu).setOnClickListener(view -> {
            AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
            alertDialog.setTitle("تأكيد الخروج");
            alertDialog.setMessage("بالتاكيد الخروج من التطبيق ");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "خروج", (dialog, which) -> finish());
            alertDialog.show();
        });



        ChipNavigationBar morphBottomNavigationView = findViewById(R.id.bottomNavigationView);
        morphBottomNavigationView.setOnItemSelectedListener(i -> {
            if (i == R.id.adds) {
                FragmentHelper.addFragment(ClerkMainActivity.this, new FragmentAds(), "FragmentAds");
            } else if (i == R.id.add_ads) {
                FragmentHelper.addFragment(ClerkMainActivity.this, new FragmentAddAds(), "FragmentAddAds");
            }
        });


        morphBottomNavigationView.setItemSelected(R.id.center, true);


    }
}
