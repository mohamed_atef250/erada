package just.innovates.twahod;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.base.DataBaseHelper;


public class FragmentAds extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_ads, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);

        AdsAdapter adsAdapter = new AdsAdapter(getActivity(), DataBaseHelper.getDataLists().ads);
        menuList.setAdapter(adsAdapter);



        return rootView;
    }


}
