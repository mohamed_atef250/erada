package just.innovates.twahod;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import just.innovates.twahod.admin.FragmentAddCenter;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.FragmentHelper;
import just.innovates.twahod.base.models.Ads;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.ViewHolder> {

    Context context;
    ArrayList<Ads> adsArrayList;

    public AdsAdapter(Context context, ArrayList<Ads>adsArrayList) {
        this.context = context;
        this.adsArrayList=adsArrayList;
    }


    @Override
    public AdsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads, parent, false);
        AdsAdapter.ViewHolder viewHolder = new AdsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdsAdapter.ViewHolder holder, final int position) {
        holder.name.setText(adsArrayList.get(position).title);
        holder.details.setText(adsArrayList.get(position).details);
        ConnectionHelper.loadImage(holder.image,adsArrayList.get(position).image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentAddAds addCenter = new FragmentAddAds();
                Bundle bundle = new Bundle();
                bundle.putSerializable("ad",adsArrayList.get(position));
                addCenter.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(),addCenter,"addCenter");
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد المسح")
                        .setContentText("بالتاكيد مسح هذا المركز ؟")
                        .setConfirmText("نعم امسح").setConfirmClickListener(sweetAlertDialog -> {
                    DataBaseHelper.removeAd(adsArrayList.get(position));
                    adsArrayList.remove(position);
                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,details;
        ImageView image,delete;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.name);
            this.details=itemView.findViewById(R.id.details);
            this.image=itemView.findViewById(R.id.image);
            this.delete=itemView.findViewById(R.id.delete);
        }
    }
}
