package just.innovates.twahod;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import just.innovates.twahod.admin.AdminMainActivity;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.SweetDialogs;
import just.innovates.twahod.base.models.Child;
import just.innovates.twahod.base.models.User;
import just.innovates.twahod.children.ChildMainActivity;
import just.innovates.twahod.mother.MotherMainActivity;
import just.innovates.twahod.specialization.SpecializationMainActivity;


public class  LoginActivity extends AppCompatActivity {
    EditText type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);
        type = findViewById(R.id.type);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},2342);
        }

        final EditText email = findViewById(R.id.email);
        final EditText password = findViewById(R.id.password);

        findViewById(R.id.register).setOnClickListener(view -> {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        });

        type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> names = new ArrayList<>();
                names.add("ام");
                names.add("مسئول التطبيق");
                names.add("متخصص");
                names.add("موظف");

                ArrayList<PopUpItem> popUpMenus=new ArrayList<>();
                for(int i=0; i<names.size(); i++){
                    popUpMenus.add(new PopUpItem(i,names.get(i)));
                }
                PopUpMenus.showPopUp(LoginActivity.this,type,popUpMenus).setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        type.setText(menuItem.getTitle());
                        return false;
                    }
                });
            }
        });


        findViewById(R.id.cv_review_reviewer_image).setOnClickListener(view -> startActivity(new Intent(LoginActivity.this, ChildMainActivity.class)));

        findViewById(R.id.btn).setOnClickListener(view -> {
            if(email.getText().toString().equals("clerk")){
                startActivity(new Intent(LoginActivity.this, ClerkMainActivity.class));
            }
            else if(email.getText().toString().equals("admin")){
                startActivity(new Intent(LoginActivity.this, AdminMainActivity.class));
            }else {

                User user = DataBaseHelper.loginUser(email.getText().toString()
                        , password.getText().toString());
                if (user != null) {
                    DataBaseHelper.saveUser(user);
                    Intent intent = new Intent(LoginActivity.this, MotherMainActivity.class);

                    if(user.type.equals("special")){
                          intent = new Intent(LoginActivity.this, SpecializationMainActivity.class);
                    }

                    startActivity(intent);


                    finish();
                } else {
                    SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                }
            }



        });


    }
}
