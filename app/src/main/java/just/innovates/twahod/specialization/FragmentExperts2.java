package just.innovates.twahod.specialization;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.mother.ExpertsAdapter;
import just.innovates.twahod.mother.FragmentAddExpert;


public class FragmentExperts2 extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_experts, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        ExpertsAdapter menuAdapter = new ExpertsAdapter(getActivity());
        menuList.setAdapter(menuAdapter);

        rootView.findViewById(R.id.tv_systems_add).setVisibility(View.GONE);


        return rootView;
    }


}
