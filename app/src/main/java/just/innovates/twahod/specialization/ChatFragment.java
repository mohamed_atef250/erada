package just.innovates.twahod.specialization;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.NotificationUtils;
import just.innovates.twahod.base.models.Chat;
import just.innovates.twahod.base.models.User;


public class ChatFragment extends Fragment {
    private View rootView;
    private RecyclerView menuList;
    private EditText message;
    private Button send;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        message = rootView.findViewById(R.id.et_login_first_name);
        send = rootView.findViewById(R.id.btn_login_sign_in);
        menuList = rootView.findViewById(R.id.menu_list);


        ChatAdapter chatAdapter = new ChatAdapter(getActivity(), getArguments().getInt("id"));
        menuList.setAdapter(chatAdapter);

        send.setOnClickListener(view -> {
            User  user = DataBaseHelper.findUser(getArguments().getInt("id"));
            User specialization  = DataBaseHelper.getSavedUser();
            Chat chat = new Chat(specialization , user, message.getText().toString(), "admin");
            DataBaseHelper.addChat(chat);
            chatAdapter.addChat(chat);
            Toast.makeText(getActivity(), "تم ارسال الرساله بنجاح", Toast.LENGTH_SHORT).show();
            NotificationUtils.showNotification(getActivity(), "رساله جديده", message.getText().toString(), new Intent());
            message.setText("");
        });

        return rootView;
    }


}
