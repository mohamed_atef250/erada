package just.innovates.twahod.specialization;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.children.FragmentVideos;
import just.innovates.twahod.mother.FragmentMotherVideos;
import just.innovates.twahod.mother.MotherMainActivity;


public class SpecializationMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        findViewById(R.id.iv_main_menu).setOnClickListener(view -> {
            AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
            alertDialog.setTitle("تأكيد الخروج");
            alertDialog.setMessage("بالتاكيد الخروج من التطبيق ");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "خروج", (dialog, which) -> finish());
            alertDialog.show();
        });




        ChipNavigationBar morphBottomNavigationView = findViewById(R.id.bottomNavigationView);
        morphBottomNavigationView.setOnItemSelectedListener(i -> {
            if (i == R.id.expert) {
                Bundle bundle = new Bundle();
                bundle.putString("type","courses");

                FragmentMotherVideos fragmentVideos=  new FragmentMotherVideos();
                fragmentVideos.setArguments(bundle);
                FragmentHelper.replaceFragment(SpecializationMainActivity.this,fragmentVideos , "FragmentCenters");
            } else if (i == R.id.special) {
                FragmentHelper.replaceFragment(SpecializationMainActivity.this, new ChattersFragment(), "FragmentVideos");

            }
        });


        morphBottomNavigationView.setItemSelected(R.id.expert, true);




    }
}
