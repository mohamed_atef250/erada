package just.innovates.twahod.specialization;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Chat;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class ChattersAdapter extends RecyclerView.Adapter<ChattersAdapter.ViewHolder> {

    Context context;
    ArrayList<Chat> chats;

    public ChattersAdapter(Context context) {
        this.context = context;
        chats = new ArrayList<>();
        ArrayList<Chat> chatsTemp = DataBaseHelper.getDataLists().chats;

        if (chatsTemp != null) {
            for (int i = 0; i < chatsTemp.size(); i++) {
                try {
                    if (!foundBefore(chatsTemp.get(i).user.id)) {
                        chats.add(chatsTemp.get(i));
                    }
                }catch (Exception e){
                    e.getStackTrace();
                }
            }
        }
    }

    public boolean foundBefore(int UserId){
        for (int i = 0; i < chats.size(); i++) {
            if (chats.get(i).user.id == UserId ) {

                return true;

            }
        }
        return  false;
    }
    public void addChat(Chat chat) {
        chats.add(chat);
        notifyDataSetChanged();
    }


    @Override
    public ChattersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_specialist2, parent, false);
        ChattersAdapter.ViewHolder viewHolder = new ChattersAdapter.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChattersAdapter.ViewHolder holder, final int position) {

        holder.message.setText(chats.get(position).admin.name);

        if (chats.get(position).type.equals("user")) {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ConnectionHelper.loadImage(holder.image, chats.get(position).user.image);
        }else {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            ConnectionHelper.loadImage(holder.image, chats.get(position).admin.image);

        }
        holder.itemView.setOnClickListener(view -> {
            ChatFragment chatFragment = new ChatFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("id", chats.get(position).admin.id);
            chatFragment.setArguments(bundle);
            FragmentHelper.replaceFragment(holder.itemView.getContext(), chatFragment, "ChatFragment");
        });
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
        }
    }
}
