package just.innovates.twahod;

import android.Manifest;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA},2342);


        new AsyncTask<Void, Void, String>(){

            @Override
            protected String doInBackground(Void... params) {
                for (int i = 0; i < 5; i++) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        // we were cancelled, stop sleeping!
                    }
                }
                return "Executed";
            }

            @Override
            protected void onPostExecute(String result) {

                Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        }.execute();

    }
}
