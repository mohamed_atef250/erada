package just.innovates.twahod.mother;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Comment;


public class FragmentComments extends Fragment {
    private View rootView;
    private RecyclerView menuList;

    TextView add;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_comments, container, false);

        menuList = rootView.findViewById(R.id.rv_systems_list);
        add = rootView.findViewById(R.id.tv_systems_add);

        CommentsAdapter menuAdapter = new CommentsAdapter(getActivity(),getArguments().getString("id"));
        menuList.setAdapter(menuAdapter);

        add.setOnClickListener(view -> showCommentDialog());


        return rootView;
    }


    private void showCommentDialog(){
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        final EditText edittext = new EditText(getActivity());

        alert.setMessage("من فضلك اترك تعليقك");
        alert.setTitle("اضافه تعليق");

        alert.setView(edittext);

        alert.setPositiveButton("اضافه", (dialog12, whichButton) -> {
            String YouEditTextValue = edittext.getText().toString();
            Comment comment = new Comment(DataBaseHelper.getCounter()+"",YouEditTextValue,DataBaseHelper.getSavedUser(),getArguments().getString("id"));
            DataBaseHelper.addComment(comment);
            CommentsAdapter menuAdapter = new CommentsAdapter(getActivity(),getArguments().getString("id"));
            menuList.setAdapter(menuAdapter);
            dialog12.dismiss();
            dialog12.cancel();
        });

        alert.setNegativeButton("خروج", (dialog1, whichButton) -> {
            dialog1.dismiss();
            dialog1.cancel();
        });



        alert.show();
    }

}
