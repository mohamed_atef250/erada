package just.innovates.twahod.mother;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.admin.FragmentChildren;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Child;
import just.innovates.twahod.base.models.Expert;


public class FragmentAddExpert extends Fragment {
    private View rootView;
    EditText title,details;
    Button add;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_expert, container, false);
        title=rootView.findViewById(R.id.title);
        details=rootView.findViewById(R.id.details);
        add = rootView.findViewById(R.id.add);

        add.setOnClickListener(view -> {
            DataBaseHelper.addExpert(new Expert(DataBaseHelper.getCounter()+"",title.getText().toString(), details.getText().toString(),  DataBaseHelper.getSavedUser()));
            Toast.makeText(getActivity(), "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
            FragmentHelper.replaceFragment(getActivity(), new FragmentExperts(), "FragmentExperts");
        });



        return rootView;
    }


}
