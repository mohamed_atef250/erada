package just.innovates.twahod.mother;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import just.innovates.twahod.R;
import just.innovates.twahod.ShowLocationActivity;
import just.innovates.twahod.admin.FragmentAddCenter;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.FragmentHelper;
import just.innovates.twahod.base.models.Center;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class CentersAdapter extends RecyclerView.Adapter<CentersAdapter.ViewHolder> {

    Context context;
    ArrayList<Center>centers;
    boolean isAdmin;

    public CentersAdapter(Context context, ArrayList<Center>centers, boolean isAdmin) {
        this.context = context;
        this.centers=centers;
        this.isAdmin=isAdmin;
    }


    @Override
    public CentersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_center, parent, false);
        CentersAdapter.ViewHolder viewHolder = new CentersAdapter.ViewHolder(view);



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CentersAdapter.ViewHolder holder, final int position) {


        ConnectionHelper.loadImage(holder.image,centers.get(position).image);

        holder.name.setText(centers.get(position).name);
        holder.about.setText(centers.get(position).about);

        if(isAdmin){
            holder.delete.setVisibility(View.VISIBLE);
        }else {
            holder.delete.setVisibility(View.GONE);
        }


        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد المسح")
                        .setContentText("بالتاكيد مسح هذا المركز ؟")
                        .setConfirmText("نعم امسح").setConfirmClickListener(sweetAlertDialog -> {
                    DataBaseHelper.removeCenter(centers.get(position));
                    centers.remove(position);

                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }).show();
            }
        });

        holder.itemView.setOnClickListener(view -> {
            if(isAdmin) {
                FragmentAddCenter addCenter = new FragmentAddCenter();
                Bundle bundle = new Bundle();
                bundle.putSerializable("center", centers.get(position));
                addCenter.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(), addCenter, "addCenter");
            }else {
                Intent intent = new Intent(view.getContext(), ShowLocationActivity.class);
                intent.putExtra("lat",centers.get(position).lat);
                intent.putExtra("lng",centers.get(position).lng);
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return centers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image,delete;
        TextView name,about;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            about = itemView.findViewById(R.id.about);
            delete= itemView.findViewById(R.id.delete);

        }
    }
}
