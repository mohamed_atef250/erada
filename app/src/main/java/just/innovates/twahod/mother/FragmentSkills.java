package just.innovates.twahod.mother;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.admin.AdminSkillsAdapter;
import just.innovates.twahod.admin.FragmentGames;
import just.innovates.twahod.admin.FragmentVideos;


public class FragmentSkills extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_bags, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        Bundle bundle = new Bundle();
        bundle.putBoolean("skills",true);

        AdminSkillsAdapter menuAdapter = new AdminSkillsAdapter(getActivity());
        menuList.setAdapter(menuAdapter);

        rootView.findViewById(R.id.tv_systems_add).setOnClickListener(view -> {
            FragmentMotherVideos fragmentAddVideo =  new FragmentMotherVideos();
            bundle.putString("type","skills");
            fragmentAddVideo.setArguments(bundle);
            FragmentHelper.addFragment(getActivity(),fragmentAddVideo,"FragmentAddVideo");
        });

        rootView.findViewById(R.id.tv_add_game).setOnClickListener(view -> {
            FragmentMotherGames fragmentAddGame =  new FragmentMotherGames();
            bundle.putString("type","skills");
            fragmentAddGame.setArguments(bundle);
            FragmentHelper.addFragment(getActivity(),fragmentAddGame,"FragmentAddGame");
        });


        return rootView;
    }


}
