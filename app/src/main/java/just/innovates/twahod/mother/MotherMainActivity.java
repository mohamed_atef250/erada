package just.innovates.twahod.mother;

import android.app.AlertDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;
import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;


public class MotherMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mother_main);


        findViewById(R.id.iv_main_menu).setOnClickListener(view -> {
            AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
            alertDialog.setTitle("تأكيد الخروج");
            alertDialog.setMessage("بالتاكيد الخروج من التطبيق ");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "خروج", (dialog, which) -> finish());
            alertDialog.show();
        });



        ChipNavigationBar morphBottomNavigationView = findViewById(R.id.bottomNavigationView);
        morphBottomNavigationView.setOnItemSelectedListener(i -> {
            if (i == R.id.special) {
                FragmentHelper.replaceFragment(MotherMainActivity.this, new FragmentMotherSpecializations(), "FragmentMotherSpecializations");
            } else if (i == R.id.bags) {
                FragmentHelper.replaceFragment(MotherMainActivity.this, new FragmentBags(), "FragmentBags");
            } else if (i == R.id.skills) {
                FragmentHelper.replaceFragment(MotherMainActivity.this, new FragmentSkills (), "FragmentVideos");
            } else if (i == R.id.center) {
                FragmentHelper.replaceFragment(MotherMainActivity.this, new FragmentMotherCenters(), "FragmentCenters");
            }else if (i == R.id.expert) {
                Bundle bundle = new Bundle();
                bundle.putString("type","courses");

                FragmentMotherVideos fragmentVideos=  new FragmentMotherVideos();
                fragmentVideos.setArguments(bundle);
                FragmentHelper.replaceFragment(MotherMainActivity.this,fragmentVideos , "FragmentCenters");
            }
        });


        morphBottomNavigationView.setItemSelected(R.id.center, true);


    }
}
