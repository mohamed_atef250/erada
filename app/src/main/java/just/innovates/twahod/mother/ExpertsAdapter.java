package just.innovates.twahod.mother;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Expert;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class ExpertsAdapter extends RecyclerView.Adapter<ExpertsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Expert> experts;

    public ExpertsAdapter(Context context) {
        this.context = context;

        experts = DataBaseHelper.getDataLists().experts;

        if (experts == null) {
            experts = new ArrayList<>();
        }
    }


    @NotNull
    @Override
    public ExpertsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expert, parent, false);
        ExpertsAdapter.ViewHolder viewHolder = new ExpertsAdapter.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NotNull ExpertsAdapter.ViewHolder holder, final int position) {

//        holder.motherName.setText(experts.get(position).user.name);
//        holder.title.setText(experts.get(position).title);
//        holder.details.setText(experts.get(position).details);
//        ConnectionHelper.loadImage(holder.image, experts.get(position).user.image);
//
//        holder.itemView.setOnClickListener(view -> {
//            FragmentComments fragmentComments = new FragmentComments();
//            Bundle bundle = new Bundle();
//            bundle.putString("id", experts.get(position).id);
//            fragmentComments.setArguments(bundle);
//            FragmentHelper.addFragment(view.getContext(), fragmentComments, "FragmentComments");
//        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView motherName, title, details;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            motherName = itemView.findViewById(R.id.tv_review_reviewer);
            title = itemView.findViewById(R.id.tv_review_distance);
            details = itemView.findViewById(R.id.tv_review_price);
            image = itemView.findViewById(R.id.image);

        }
    }
}
