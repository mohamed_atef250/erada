package just.innovates.twahod.mother;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Comment;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private Context context;

    private ArrayList<Comment> comments;

    public CommentsAdapter(Context context, String id) {
        this.context = context;
        ArrayList<Comment> commentsTemp;
        comments = new ArrayList<>();
        commentsTemp = DataBaseHelper.getDataLists().comments;
        if (commentsTemp != null) {
            for (int i = 0; i < commentsTemp.size(); i++) {
                if (commentsTemp.get(i).postId.equals(id)) {
                    comments.add(commentsTemp.get(i));
                }
            }

        }

    }


    @NotNull
    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        CommentsAdapter.ViewHolder viewHolder = new CommentsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder holder, final int position) {
        holder.relativeLayout.setBackgroundColor(context.getResources().getColor((position % 2 == 0) ? R.color.colorWhite : R.color.colorGrayLight));
        holder.name.setText(comments.get(position).user.name);
        holder.comment.setText(comments.get(position).comment);
        ConnectionHelper.loadImage(holder.image, comments.get(position).user.image);
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relativeLayout;
        TextView name, comment;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.rl_chat_messaage);
            name = itemView.findViewById(R.id.tv_review_reviewer);
            comment = itemView.findViewById(R.id.tv_review_price);
            image = itemView.findViewById(R.id.image);
        }
    }
}
