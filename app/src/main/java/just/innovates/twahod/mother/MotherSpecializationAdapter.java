package just.innovates.twahod.mother;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import just.innovates.twahod.R;

import just.innovates.twahod.admin.FragmentAddSpecialist;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.FragmentHelper;
import just.innovates.twahod.base.models.User;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;
import just.innovates.twahod.specialization.ChatFragment;


public class MotherSpecializationAdapter  extends RecyclerView.Adapter<MotherSpecializationAdapter.ViewHolder> {

    Context context;
    ArrayList<User>users;

    public MotherSpecializationAdapter(Context context,ArrayList<User>users) {
        this.context = context;
        this.users=users;

    }


    @Override
    public MotherSpecializationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_specialist, parent, false);
        MotherSpecializationAdapter.ViewHolder viewHolder = new MotherSpecializationAdapter.ViewHolder(view);



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MotherSpecializationAdapter.ViewHolder holder, final int position) {



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MotherChatFragment motherChatFragment = new MotherChatFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("id",users.get(position).id);
                motherChatFragment.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(),motherChatFragment,"MotherChatFragment");
            }
        });

        ConnectionHelper.loadImage(holder.image,users.get(position).image);
        holder.name.setText(users.get(position).name);
        holder.special.setText(users.get(position).special);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name,special;
        Button callphone;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            special = itemView.findViewById(R.id.special);
            image = itemView.findViewById(R.id.image);
            callphone= itemView.findViewById(R.id.callphone);

        }
    }
}

