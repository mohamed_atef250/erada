package just.innovates.twahod.mother;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Chat;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class MotherChatAdapter extends RecyclerView.Adapter<MotherChatAdapter.ViewHolder> {

    Context context;
    ArrayList<Chat> chats;

    public MotherChatAdapter(Context context, int userID) {
        this.context = context;
        chats = new ArrayList<>();
        ArrayList<Chat> chatsTemp = DataBaseHelper.getDataLists().chats;
        if (chatsTemp != null) {
            for (int i = 0; i < chatsTemp.size(); i++) {
                try {
                    if (chatsTemp.get(i).user.id==userID) {
                        chats.add(chatsTemp.get(i));
                    }
                }catch (Exception e){
                    e.getStackTrace();
                }
            }
        }
    }

    public void addChat(Chat chat) {
        chats.add(chat);
        notifyDataSetChanged();
    }


    @Override
    public MotherChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        MotherChatAdapter.ViewHolder viewHolder = new MotherChatAdapter.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MotherChatAdapter.ViewHolder holder, final int position) {

        holder.chatterLayout.setLayoutDirection(chats.get(position).type.equals("user") ? View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        holder.message.setText(chats.get(position).message);
        holder.itemView.setOnClickListener(view -> {

        });

        if(chats.get(position).type.equals("user")){
            ConnectionHelper.loadImage(holder.image,chats.get(position).user.image);
        }else {
            ConnectionHelper.loadImage(holder.image,chats.get(position).admin.image);
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.iv_doctor);
        }
    }
}
