package just.innovates.twahod.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;


public class FragmentGames extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_games, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        GamesAdapter menuAdapter = new GamesAdapter(getActivity(),getArguments().getString("type"),false);
        menuList.setAdapter(menuAdapter);


        rootView.findViewById(R.id.tv_systems_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentAddGame fragmentAddGame = new FragmentAddGame();
                fragmentAddGame.setArguments(getArguments());
                FragmentHelper.addFragment(getActivity(),fragmentAddGame,"FragmentAddGame");
            }
        });

        return rootView;
    }


}
