package just.innovates.twahod.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.Validate;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.SweetDialogs;
import just.innovates.twahod.base.filesutils.FileOperations;
import just.innovates.twahod.base.filesutils.VolleyFileObject;
import just.innovates.twahod.base.models.Child;
import just.innovates.twahod.base.models.ImageResponse;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;
import just.innovates.twahod.base.volleyutils.ConnectionListener;

import static android.view.View.*;


public class FragmentAddChild extends Fragment {
    private View rootView;
    private EditText name, userName, email, phone, state, type, password;
    private Button add;
    private ImageView image;
    String selectedImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_children, container, false);
        name = rootView.findViewById(R.id.name);
        userName = rootView.findViewById(R.id.user_name);
        email = rootView.findViewById(R.id.email);
        phone = rootView.findViewById(R.id.phone);
        state = rootView.findViewById(R.id.state);
        type = rootView.findViewById(R.id.type);
        password = rootView.findViewById(R.id.password);
        image = rootView.findViewById(R.id.image);

        add = rootView.findViewById(R.id.btn);

        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        add.setOnClickListener(view -> {
            if(validate()) {
                DataBaseHelper.addChild(new Child(DataBaseHelper.getCounter() + "", name.getText().toString(), userName.getText().toString(), email.getText().toString(), phone.getText().toString(), state.getText().toString(), type.getText().toString(), password.getText().toString(), selectedImage));
                Toast.makeText(getActivity(), "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
                FragmentHelper.replaceFragment(getActivity(), new FragmentChildren(), "FragmentChildren");
            }
        });

        return rootView;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            43);
            volleyFileObjects.add(volleyFileObject);
            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }




    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())
                ||Validate.isEmpty(email.getText().toString())
                ||Validate.isEmpty(phone.getText().toString())  ){
            SweetDialogs.errorMessage(getActivity(),"من فضلك قم بملآ جميع البيانات");
            return false;

        }else if(!Validate.isAvLen(password.getText().toString(),7,100)){
            SweetDialogs.errorMessage(getActivity(),"كلمة المرور يجب ان تكون اكبر من ٦ حروف او ارقام ");
            return  false;
        }


        return true;
    }





    private void addServiceApi() {
      final   ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();
                ConnectionHelper.loadImage(image,selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }


}
