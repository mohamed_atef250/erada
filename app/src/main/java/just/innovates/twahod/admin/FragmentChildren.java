package just.innovates.twahod.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;

import static android.view.View.*;


public class FragmentChildren extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_children, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        ChildrenAdapter menuAdapter = new ChildrenAdapter(getActivity());
        menuList.setAdapter(menuAdapter);

        rootView.findViewById(R.id.tv_systems_add).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(getActivity(),new FragmentAddChild(),"FragmentAddChild");
            }
        });


        return rootView;
    }


}
