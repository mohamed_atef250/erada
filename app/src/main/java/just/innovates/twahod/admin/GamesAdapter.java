package just.innovates.twahod.admin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Game;
import just.innovates.twahod.base.models.Video;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;
import just.innovates.twahod.children.GameDetailsFragment;


public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.ViewHolder> {

    Context context;
    ArrayList<Game> games;
    boolean isAdmin;
    public GamesAdapter(Context context,String type,boolean isAdmin) {
        this.context = context;
        games = DataBaseHelper.getDataLists().games;
        ArrayList<Game> gamesTemp= DataBaseHelper.getDataLists().games;
        games = new ArrayList<>();
        this.isAdmin=isAdmin;
        if (gamesTemp != null) {
            games = new ArrayList<>();

            for(int i=0; i<gamesTemp.size(); i++){
                if(gamesTemp.get(i).type.equals(type)){

                    games.add(gamesTemp.get(i));
                }
            }
        }
    }


    @Override
    public GamesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game, parent, false);
        GamesAdapter.ViewHolder viewHolder = new GamesAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GamesAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.image, games.get(position).image);
        holder.name.setText(games.get(position).name);



        holder.itemView.setOnClickListener(view -> {
            GameDetailsFragment gameDetailsFragment = new GameDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("game", games.get(position));
            gameDetailsFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(), gameDetailsFragment, "ChatFragment");
        });

        if(isAdmin){
            holder.delete.setVisibility(View.VISIBLE);
        }else {
            holder.delete.setVisibility(View.GONE);
        }

        holder.delete.setOnClickListener(view -> {
            AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
            alertDialog.setTitle("تأكيد الحذف");
            alertDialog.setMessage("بالتاكيد هل تريد حذف "+games.get(position).name);

            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "نعم  احذف", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(view.getContext(), "تمت العملية بنجاح", Toast.LENGTH_SHORT).show();

                    DataBaseHelper.removeGame(games.get(position));
                    games.remove(position);
                    notifyDataSetChanged();

                }
            });



            alertDialog.show();
        });
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image,delete;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            delete = itemView.findViewById(R.id.delete);

        }
    }
}
