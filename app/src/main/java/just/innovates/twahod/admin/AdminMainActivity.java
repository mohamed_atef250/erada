package just.innovates.twahod.admin;

import android.app.AlertDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;
import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;


public class AdminMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FragmentHelper.replaceFragment(AdminMainActivity.this,new FragmentAdminCenters(),"FragmentAdminCenters");

        findViewById(R.id.iv_main_menu).setOnClickListener(view -> {
            AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
            alertDialog.setTitle("تأكيد الخروج");
            alertDialog.setMessage("بالتاكيد الخروج من التطبيق ");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "خروج", (dialog, which) -> finish());
            alertDialog.show();
        });



        ChipNavigationBar morphBottomNavigationView = findViewById(R.id.bottomNavigationView);
        morphBottomNavigationView.setOnItemSelectedListener(i -> {
             if(i==R.id.centers){
                FragmentHelper.replaceFragment(AdminMainActivity.this,new FragmentAdminCenters(),"FragmentAdminCenters");
            }else if(i==R.id.special){
                 FragmentHelper.replaceFragment(AdminMainActivity.this,new FragmentAdminSpecializations(),"FragmentAdminSpecializations");

             }else if(i==R.id.bags){
                 FragmentHelper.replaceFragment(AdminMainActivity.this,new FragmentAdminBags(),"FragmentAdminBags");

             }else if(i==R.id.skills){
                 FragmentHelper.replaceFragment(AdminMainActivity.this,new FragmentAdminSkills(),"FragmentAdminBags");

             }else {

                 Bundle bundle  = new Bundle();

                  FragmentVideos fragmentAddVideo =  new FragmentVideos();
                 bundle.putString("type","courses");
                 fragmentAddVideo.setArguments(bundle);
                 FragmentHelper.addFragment(AdminMainActivity.this,fragmentAddVideo,"FragmentAddVideo");
             }
        });


        morphBottomNavigationView.setItemSelected(R.id.video,true);


    }
}
