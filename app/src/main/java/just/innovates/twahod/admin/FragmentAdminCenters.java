package just.innovates.twahod.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;


public class FragmentAdminCenters extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_admin_centers, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        AdminCentersAdapter menuAdapter = new AdminCentersAdapter(getActivity(), DataBaseHelper.getDataLists().centers);
        menuList.setAdapter(menuAdapter);

     rootView.findViewById(R.id.tv_systems_add).setOnClickListener(view -> FragmentHelper.addFragment(getActivity(),new FragmentAddCenter(),"FragmentAddCenter"));


        return rootView;
    }


}
