package just.innovates.twahod.admin;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.FragmentHelper;
import just.innovates.twahod.base.models.User;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class AdminSpecializationAdapter extends RecyclerView.Adapter<AdminSpecializationAdapter.ViewHolder> {

    Context context;
    ArrayList<User>users;

    public AdminSpecializationAdapter(Context context,ArrayList<User>users) {
        this.context = context;
        this.users=users;

    }


    @Override
    public AdminSpecializationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_specialist_admin, parent, false);
        AdminSpecializationAdapter.ViewHolder viewHolder = new AdminSpecializationAdapter.ViewHolder(view);



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdminSpecializationAdapter.ViewHolder holder, final int position) {

        holder.delete.setOnClickListener(view -> {
            new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("هل تريد المسح")
                    .setContentText("بالتاكيد مسح الاستشاري ؟")
                    .setConfirmText("نعم امسح").setConfirmClickListener(sweetAlertDialog -> {
                DataBaseHelper.removeUser(users.get(position));
                users.remove(position);

                notifyDataSetChanged();

                try {
                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();
                }catch (Exception e){
                    e.getStackTrace();
                }

            }).show();
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentAddSpecialist addSpecialist = new FragmentAddSpecialist();
                Bundle bundle = new Bundle();
                bundle.putSerializable("user",users.get(position));
                addSpecialist.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(),addSpecialist,"FragmentAddSpecialist");
            }
        });

        ConnectionHelper.loadImage(holder.image,users.get(position).image);
        holder.name.setText(users.get(position).name);
        holder.special.setText(users.get(position).special);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image,delete;
        TextView name,special;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            special = itemView.findViewById(R.id.special);
            image = itemView.findViewById(R.id.image);
            delete = itemView.findViewById(R.id.delete);
        }
    }
}
