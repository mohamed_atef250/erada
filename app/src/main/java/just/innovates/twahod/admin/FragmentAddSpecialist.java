package just.innovates.twahod.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.Validate;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.SweetDialogs;
import just.innovates.twahod.base.filesutils.FileOperations;
import just.innovates.twahod.base.filesutils.VolleyFileObject;
import just.innovates.twahod.base.models.ImageResponse;
import just.innovates.twahod.base.models.User;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;
import just.innovates.twahod.base.volleyutils.ConnectionListener;


public class FragmentAddSpecialist extends Fragment {
    private View rootView;
    private String selectedImage;
    private ImageView image;
    private EditText name, userName, email, phone, password,job,special;
    Button add;
    User user;
    boolean isEdit;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_specialist, container, false);


        name = rootView.findViewById(R.id.name);
        userName = rootView.findViewById(R.id.user_name);
        email = rootView.findViewById(R.id.email);
        phone = rootView.findViewById(R.id.phone);
        password = rootView.findViewById(R.id.password);
        image = rootView.findViewById(R.id.image);
        job = rootView.findViewById(R.id.job);
        special = rootView.findViewById(R.id.special);

        add = rootView.findViewById(R.id.add);





        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });


        try{
            user = (User) getArguments().getSerializable("user");
            name.setText(user.name);
            userName.setText(user.userName);
            email.setText(user.email);
            job.setText(user.job);
            special.setText(user.special);
            job.setText(user.job);
            password.setText(user.password);
            phone.setText(user.phone);
            selectedImage = user.image;
            ConnectionHelper.loadImage(image,user.image);
            add.setText("تعديل");
            isEdit=user!=null;


        }catch (Exception e){

        }

        add.setOnClickListener(view -> {
            if(validate()) {

                if(isEdit) {
                    User user2 = new User(name.getText().toString(), userName.getText().toString(),
                            phone.getText().toString(),
                            email.getText().toString(),
                            password.getText().toString(),
                            selectedImage, "special");
                    user2.job = job.getText().toString();
                    user2.special = special.getText().toString();
                    user2.id=this.user.id;


                    DataBaseHelper.updateUser(user2);
                    Toast.makeText(getActivity(), "تمت التعديل بنجاح", Toast.LENGTH_SHORT).show();
                }else{
                    User user = new User(name.getText().toString(),
                            userName.getText().toString(), phone.getText().toString(),
                            email.getText().toString(), password.getText().toString(),
                            selectedImage, "special");

                    user.job = job.getText().toString();
                    user.special = special.getText().toString();
                    DataBaseHelper.addUser(user);
                    Toast.makeText(getActivity(), "تم  الاضافه بنجاح", Toast.LENGTH_SHORT).show();
                }



                FragmentHelper.replaceFragment(getActivity(), new FragmentAdminSpecializations(), "FragmentChildren");



            }
        });



        return rootView;
    }


    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())
                ||Validate.isEmpty(email.getText().toString())
                ||Validate.isEmpty(phone.getText().toString())
                ||Validate.isEmpty(password.getText().toString())) {
            SweetDialogs.errorMessage(getActivity(), "من فضلك قم بملآ جميع البيانات");
            return false;
        }
//        }else if(!Validate.isAvLen(password.getText().toString(),7,100)){
//            SweetDialogs.errorMessage(getActivity(),"كلمة المرور يجب ان تكون اكبر من ٦ حروف او ارقام ");
//            return  false;
//        }else if(DataBaseHelper.findUserCheck(email.getText().toString())){
//            SweetDialogs.errorMessage(getActivity(),"هذا المستخدم موجود من قبل");
//            return  false;
//        }


        return true;
    }



    private ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            43);
            volleyFileObjects.add(volleyFileObject);
            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();
                ConnectionHelper.loadImage(image,selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }




}
