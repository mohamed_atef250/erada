package just.innovates.twahod.admin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Child;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class ChildrenAdapter extends RecyclerView.Adapter<ChildrenAdapter.ViewHolder> {

    Context context;
    ArrayList<Child> children;

    public ChildrenAdapter(Context context) {
        this.context = context;

        children = DataBaseHelper.getDataLists().children;

        if (children == null) {
            children = new ArrayList<>();
        }
    }


    @Override
    public ChildrenAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child, parent, false);
        ChildrenAdapter.ViewHolder viewHolder = new ChildrenAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChildrenAdapter.ViewHolder holder, final int position) {
        ConnectionHelper.loadImage(holder.image, "https://www.almrsal.com/wp-content/uploads/2018/05/%D9%85%D8%AA%D9%84%D8%A7%D8%B2%D9%85%D8%A9-%D8%AF%D8%A7%D9%88%D9%86.jpg");
//        holder.name.setText(children.get(position).name);
//
//        holder.delete.setOnClickListener(view -> {
//            AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
//            alertDialog.setTitle("تأكيد الحذف");
//            alertDialog.setMessage("بالتاكيد هل تريد حذف "+children.get(position).name);
//
//            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "نعم  احذف", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(view.getContext(), "تمت العملية بنجاح", Toast.LENGTH_SHORT).show();
//
//                    DataBaseHelper.removeJob(children.get(position));
//                    children.remove(position);
//                    notifyDataSetChanged();
//
//                }
//            });
//
//            alertDialog.show();
//        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView image,delete;;


        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
            delete = itemView.findViewById(R.id.delete);


        }
    }
}
