package just.innovates.twahod.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.User;


public class FragmentAdminSpecializations extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_admin_specialists, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);

        CardView cardView = rootView.findViewById(R.id.cardView);
        cardView.setVisibility(View.GONE);

        ArrayList<User> tempUsers = new ArrayList<>();
        ArrayList<User>users = DataBaseHelper.getDataLists().users;

        for(int i=0; i<users.size(); i++){
            if(users.get(i).type.equals("special"))
            tempUsers.add(users.get(i));
        }
        AdminSpecializationAdapter menuAdapter = new AdminSpecializationAdapter(getActivity(), tempUsers);
        menuList.setAdapter(menuAdapter);

        rootView.findViewById(R.id.tv_systems_add).setOnClickListener(view -> FragmentHelper.addFragment(getActivity(),new FragmentAddSpecialist(),"FragmentAddSpecialist"));


        return rootView;
    }


}
