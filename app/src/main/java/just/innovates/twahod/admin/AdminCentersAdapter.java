package just.innovates.twahod.admin;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import cn.pedant.SweetAlert.SweetAlertDialog;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.FragmentHelper;
import just.innovates.twahod.base.models.Center;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class AdminCentersAdapter extends RecyclerView.Adapter<AdminCentersAdapter.ViewHolder> {

    Context context;
    ArrayList<Center>centers;


    public AdminCentersAdapter(Context context,ArrayList<Center>centers) {
        this.context = context;
        this.centers=centers;
    }


    @Override
    public AdminCentersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_center, parent, false);
        AdminCentersAdapter.ViewHolder viewHolder = new AdminCentersAdapter.ViewHolder(view);



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdminCentersAdapter.ViewHolder holder, final int position) {


        ConnectionHelper.loadImage(holder.image,centers.get(position).image);

        holder.name.setText(centers.get(position).name);
        holder.about.setText(centers.get(position).about);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد المسح")
                        .setContentText("بالتاكيد مسح هذا المركز ؟")
                        .setConfirmText("نعم امسح").setConfirmClickListener(sweetAlertDialog -> {
                    DataBaseHelper.removeCenter(centers.get(position));
                    centers.remove(position);

                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }).show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentAddCenter addCenter = new FragmentAddCenter();
                Bundle bundle = new Bundle();
                bundle.putSerializable("center",centers.get(position));
                addCenter.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(),addCenter,"addCenter");
            }
        });

    }

    @Override
    public int getItemCount() {
        return centers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image,delete;
        TextView name,about;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            about = itemView.findViewById(R.id.about);
            delete= itemView.findViewById(R.id.delete);

        }
    }
}
