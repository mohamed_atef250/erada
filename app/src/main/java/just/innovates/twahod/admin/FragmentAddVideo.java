package just.innovates.twahod.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Child;
import just.innovates.twahod.base.models.Video;


public class FragmentAddVideo extends Fragment {
    private View rootView;
    private EditText youtube,name;
    private Button add;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_video, container, false);

        youtube = rootView.findViewById(R.id.youtube);
        name = rootView.findViewById(R.id.name);
        add = rootView.findViewById(R.id.btn);

        add.setOnClickListener(view -> {
            Video video= new Video(youtube.getText().toString(),name.getText().toString());

            try{
                if(getArguments().getBoolean("skills",false))
                video.type="skill";
            }catch (Exception e){
                    e.getStackTrace();
            }

            try{
                if(getArguments().getBoolean("courses",false))
                    video.type="courses";
            }catch (Exception e){
                e.getStackTrace();
            }

            video.type=getArguments().getString("type");
            DataBaseHelper.addVideo(video);
            Toast.makeText(getActivity(), "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
            FragmentVideos fragmentVideos = new FragmentVideos();
            fragmentVideos.setArguments(getArguments());
            FragmentHelper.replaceFragment(getActivity(), fragmentVideos, "FragmentVideos");
        });

        return rootView;
    }


}
