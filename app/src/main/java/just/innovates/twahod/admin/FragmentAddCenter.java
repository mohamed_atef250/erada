package just.innovates.twahod.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;
import just.innovates.twahod.SelectLocationActivity;
import just.innovates.twahod.Validate;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.SweetDialogs;
import just.innovates.twahod.base.filesutils.FileOperations;
import just.innovates.twahod.base.filesutils.VolleyFileObject;
import just.innovates.twahod.base.models.Center;
import just.innovates.twahod.base.models.ImageResponse;
import just.innovates.twahod.base.models.User;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;
import just.innovates.twahod.base.volleyutils.ConnectionListener;


public class
FragmentAddCenter extends Fragment {
    private View rootView;
    private String selectedImage;

    private EditText name, city, location, about,image;
    public double lat, lng;
    Button add;

    boolean isEdit=false;
    Center center;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_center, container, false);

        name =     rootView.findViewById(R.id.name);
        city =     rootView.findViewById(R.id.city);
        location = rootView.findViewById(R.id.location);
        about =    rootView.findViewById(R.id.about);

        location.setFocusable(false);
        location.setClickable(true);

        location.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), SelectLocationActivity.class);
            startActivityForResult(intent, 321);
        });


        try{
            center = (Center) getArguments().getSerializable("center");

            name.setText(center.name);
            about.setText(center.about);
            selectedImage = center.image;
            city.setText(center.city);
            lat=center.lat;
            lng=center.lng;
            location.setText("تغير الموقع");

            if(center!=null){
                isEdit = true;
            }

        }catch (Exception e){
            e.getStackTrace();
        }




        image = rootView.findViewById(R.id.image);
        image.setFocusable(false);
        image.setClickable(true);

        add = rootView.findViewById(R.id.add);

        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });


        add.setOnClickListener(view -> {
            if (validate()) {

                if(isEdit){
                    Center center =    new Center(
                            name.getText().toString(),
                            city.getText().toString(),
                            about.getText().toString(),
                            lat,
                            lng,
                            selectedImage);
                    center.id = this.center.id;
                    DataBaseHelper.updateCenter(center);
                    Toast.makeText(getActivity(), "تمت التعديل بنجاح", Toast.LENGTH_SHORT).show();
                }else {
                    DataBaseHelper.addCenter(new Center(
                            name.getText().toString(),
                            city.getText().toString(),
                            about.getText().toString(),
                            lat,
                            lng,
                            selectedImage));
                    Toast.makeText(getActivity(), "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
                }

                FragmentHelper.replaceFragment(getActivity(), new FragmentAdminCenters(), "FragmentAdminCenters");
            }
        });


        return rootView;
    }


    private boolean validate() {
        if (Validate.isEmpty(name.getText().toString())
                || Validate.isEmpty(city.getText().toString())
                || Validate.isEmpty(location.getText().toString())
                || Validate.isEmpty(about.getText().toString())) {
            SweetDialogs.errorMessage(getActivity(), "من فضلك قم بملآ جميع البيانات");
            return false;

        }


        return true;
    }


    private ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 321) {
            location.setText("تم اختيار الموقع بنجاح");
            lat = data.getDoubleExtra("lat", 0.0);
            lng = data.getDoubleExtra("lng", 0.0);
        } else {
            try {
                volleyFileObjects = new ArrayList<>();
                VolleyFileObject volleyFileObject =
                        FileOperations.getVolleyFileObject(getActivity(), data, "image",
                                43);
                volleyFileObjects.add(volleyFileObject);
                image.setText("تم اختيار الصوره بنجاح");
                addServiceApi();
            } catch (Exception E) {
                E.getStackTrace();
            }
        }

    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }


}
