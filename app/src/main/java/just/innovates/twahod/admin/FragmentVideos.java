package just.innovates.twahod.admin;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.twahod.FragmentHelper;
import just.innovates.twahod.R;


public class FragmentVideos extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_videos, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);



        VideosAdapter menuAdapter = new VideosAdapter(getActivity(),getArguments().getString("type"),false);
        menuList.setAdapter(menuAdapter);

        rootView.findViewById(R.id.tv_systems_add).setOnClickListener(view -> {
            FragmentAddVideo fragmentAddVideo = new FragmentAddVideo();
            fragmentAddVideo.setArguments(getArguments());
            FragmentHelper.addFragment(getActivity(), fragmentAddVideo,"FragmentAddVideo");
        });

        return rootView;
    }


}
