package just.innovates.twahod.admin;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import just.innovates.twahod.R;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.models.Video;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;


public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Video> videos;
    boolean isAdmin;
    String type;

    public VideosAdapter(Context context,String type,    boolean isAdmin) {
        this.context = context;
        this.type=type;
        this.isAdmin=isAdmin;

        ArrayList<Video> videosTemp= DataBaseHelper.getDataLists().videos;
        videos = new ArrayList<>();

        if (videosTemp != null) {
            videos = new ArrayList<>();

            for(int i=0; i<videosTemp.size(); i++){
                if(videosTemp.get(i).type.equals(type)){

                    videos.add(videosTemp.get(i));
                }
            }
        }
    }


    @Override
    public VideosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);
        VideosAdapter.ViewHolder viewHolder = new VideosAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VideosAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.image, "https://img.youtube.com/vi/" +getYouTubeId(videos.get(position).video)+ "/hqdefault.jpg");
        holder.delete.setVisibility(View.VISIBLE);
        holder.name.setText(videos.get(position).name);

        if(isAdmin){
            holder.delete.setVisibility(View.VISIBLE);
        }else {
            holder.delete.setVisibility(View.GONE);
        }
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد المسح")
                        .setContentText("بالتاكيد مسح الفيديو ؟")
                        .setConfirmText("نعم امسح").setConfirmClickListener(sweetAlertDialog -> {
                    DataBaseHelper.removeVideo(videos.get(position));
                    videos.remove(position);

                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }).show();
            }
        });

        holder.itemView.setOnClickListener(view -> watchYoutubeVideo(view.getContext(), getYouTubeId(videos.get(position).video)));
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image,delete;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            delete = itemView.findViewById(R.id.delete);
            name = itemView.findViewById(R.id.name);
        }
    }

    private String getYouTubeId(String youTubeUrl) {
        String pattern = "https?://(?:[0-9A-Z-]+\\.)?(?:youtu\\.be/|youtube\\.com\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w]*(?:['\"][^<>]*>|</a>))[?=&+%\\w]*";

        Pattern compiledPattern = Pattern.compile(pattern,
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }
}
