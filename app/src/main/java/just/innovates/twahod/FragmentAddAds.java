package just.innovates.twahod;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.SweetDialogs;
import just.innovates.twahod.base.filesutils.FileOperations;
import just.innovates.twahod.base.filesutils.VolleyFileObject;
import just.innovates.twahod.base.models.Ads;
import just.innovates.twahod.base.models.Center;
import just.innovates.twahod.base.models.ImageResponse;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;
import just.innovates.twahod.base.volleyutils.ConnectionListener;


public class FragmentAddAds  extends Fragment {
    View rootView;
    EditText name,details,image;

    boolean isEdit=false;
   public Ads ads;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_ad_ads, container, false);

        name=rootView.findViewById(R.id.name);
        details=rootView.findViewById(R.id.details);
        image = rootView.findViewById(R.id.image);
        Button addd =  rootView.findViewById(R.id.btn);

        image.setFocusable(false);
        image.setClickable(true);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });



        try{
            ads = (Ads) getArguments().getSerializable("ad");
            name.setText(ads.title);
            details.setText(ads.details);
            selectedImage = ads.image;
            image.setText("تغير الصوره");
            if(ads!=null){
                isEdit = true;
                addd.setText("تعديل");
            }

        }catch (Exception e){
            e.getStackTrace();
        }



        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){


                    if(isEdit){
                        Ads center =    new Ads(
                                name.getText().toString(),
                                details.getText().toString(),
                                selectedImage);
                        center.id = FragmentAddAds.this.ads.id;
                        DataBaseHelper.updateAds(center);
                         SweetDialogs.successMessage(requireActivity(), "تم التعديل بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                FragmentHelper.popAllFragments(getActivity());
                                FragmentHelper.addFragment(getActivity(), new FragmentAddAds(), "FragmentAddAds");



                            }
                        });
                    }else {
                        DataBaseHelper.addAds(new Ads(name.getText().toString(),details.getText().toString(),selectedImage));
                        SweetDialogs.successMessage(requireActivity(), "تم الاضافه بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                FragmentHelper.popAllFragments(getActivity());
                                FragmentHelper.addFragment(getActivity(), new FragmentAddAds(), "FragmentAddAds");



                            }
                        });
                    }



                }
            }
        });


        return rootView;
    }

    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())){
            name.setError("من فضلك املا الحقل");
            return false;
        }

        if(Validate.isEmpty(details.getText().toString())){
            details.setError("من فضلك املا الحقل");
            return false;
        }
        return true;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                FileOperations.getVolleyFileObject(getActivity(), data, "image",
                        43);


        image.setText("تم اضافه الهويه بنجاح");

        volleyFileObjects.add(volleyFileObject);


        addServiceApi();


    }


    String selectedImage = "";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta", "" + selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }



}
