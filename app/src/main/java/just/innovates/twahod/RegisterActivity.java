package just.innovates.twahod;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;

import just.innovates.twahod.admin.FragmentChildren;
import just.innovates.twahod.base.DataBaseHelper;
import just.innovates.twahod.base.SweetDialogs;
import just.innovates.twahod.base.filesutils.FileOperations;
import just.innovates.twahod.base.filesutils.VolleyFileObject;
import just.innovates.twahod.base.models.ImageResponse;
import just.innovates.twahod.base.models.User;
import just.innovates.twahod.base.volleyutils.ConnectionHelper;
import just.innovates.twahod.base.volleyutils.ConnectionListener;
import just.innovates.twahod.mother.MotherMainActivity;


public class RegisterActivity  extends AppCompatActivity {
 
    private String selectedImage;
    private ImageView image;
    private EditText name, userName, email, phone, password;
    Button add;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_register);
 


        name = findViewById(R.id.name);
        userName = findViewById(R.id.user_name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);

        password = findViewById(R.id.password);
        image = findViewById(R.id.image);

        add = findViewById(R.id.add);

        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        add.setOnClickListener(view -> {
            if(validate()){
            User user = new User(name.getText().toString(), userName.getText().toString(), phone.getText().toString(), email.getText().toString(), password.getText().toString(),selectedImage,"user");
            DataBaseHelper.addUser(user);
            DataBaseHelper.saveUser(user);
            Toast.makeText(this, "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(RegisterActivity.this, MotherMainActivity.class));
            }
        });


        
        
    }


    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())
                ||Validate.isEmpty(email.getText().toString())
                ||Validate.isEmpty(phone.getText().toString())  ){
            SweetDialogs.errorMessage(RegisterActivity.this,"من فضلك قم بملآ جميع البيانات");
            return false;

        }else if(!Validate.isAvLen(password.getText().toString(),7,100)){
            SweetDialogs.errorMessage(RegisterActivity.this,"كلمة المرور يجب ان تكون اكبر من ٦ حروف او ارقام ");
            return  false;
        }else if(DataBaseHelper.findChildCheck(email.getText().toString())){
            SweetDialogs.errorMessage(RegisterActivity.this,"هذا المستخدم موجود من قبل");
            return  false;
        }


        return true;
    }


  

    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(this, data, "image",
                            43);
            volleyFileObjects.add(volleyFileObject);
            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();
                ConnectionHelper.loadImage(image,selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }




}
